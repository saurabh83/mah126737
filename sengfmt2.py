import re, datetime
import calendar


def margin(command, s):
   
    test_list =  s.split()

    # initializing padding numbers
    lead_size = int(command) - 1
    trail_size = 1

    res = []
    for ele in test_list:

        # * operator handles number of spaces
        res.append((lead_size * ' ') + ele + (trail_size * ' '))

    afterFormat= ' '.join((res))
    
    # printing result
    return afterFormat

     
        
def maxWidth(command, s):
   
    a= [s[i:i+(int(command))] for i in range(0, len(s), (int(command)))]
    return "\n".join(a)
        
        
def replace(command, command2, s):
    
    return s.replace(command, command2)
       
       
        
def monthabbr(command, s):
   
    if command== 'on':
        try:
            match = re.search('\d{4}/\d{2}/\d{2}', s)
            date = datetime.datetime.strptime(match.group(), '%Y/%m/%d').date()
        
            
            dd= str(date)
        
            sss= str(date)[5:7]
       
            monthConvert= calendar.month_abbr[int(sss)]
    
            last = dd.replace(sss, monthConvert)
        
            beta= dd.replace('-','/')
            alpha= last.replace('-',' ')

            alphaDuo= alpha[5:8]+". "+alpha[9:11]+", "+alpha[0:4]
        except:
            print('invalid date format please enter date in yyyy/mm/dd format')
        return s.replace(beta, alphaDuo)
        
    
       